#ifndef SCENARIO_H
#define SCENARIO_H


#include "application/gmlibwrapper.h"

// qt
#include <QObject>

namespace GMlib {
  template<typename T>
  class DVector;

  template<typename T, int n>
  class PCurve;

  template<typename T, int n>
  class PSurf;

  template <typename T>
  class PCircle;

  template<typename T>
  class PBlend;

  template<typename T>
  class BSpline;

  template<typename T>
  class PClover;

  template<typename T>
  class PButterfly;

  template<typename T>
  class XFigure;

  template<typename T>
  class GERBSCurve;

  template<typename T>
  class GERBSSurf;

  class Color;

  class Material;

  enum class SimulationMode;
  enum class SurfaceShape;
}


class Scenario : public GMlibWrapper {
  Q_OBJECT
public:
  using GMlibWrapper::GMlibWrapper;

  void                                      initializeScenario() override;
  void                                      cleanupScenario() override;
  void                                      initializeCamera();

public slots:
  void                                      callDefferedGL();

private:
  void                                      simulation1();
  void                                      simulation2();
  void                                      simulation3();
  void                                      simulation4();
  void                                      simulation5();
  // Curves
  GMlib::DVector<GMlib::Vector<float, 3>>   generatePoints(int input);
  GMlib::PCurve<float,3>*                   prepareBspline(const GMlib::Color& color, int offset=0, int n=5);
  GMlib::PCurve<float,3>*                   prepareBlending(GMlib::PCurve<float,3>* &curve1, GMlib::PCurve<float,3>* &curve2, float t_blend);
  // Models
  GMlib::PCurve<float,3>*                   prepareCylindericalSineWave();
  GMlib::PCurve<float,3>*                   prepareClover();
  GMlib::PCurve<float,3>*                   prepareRose();
  GMlib::PCurve<float,3>*                   prepareReef();
  GMlib::PCurve<float,3>*                   prepareAsteroid();
  GMlib::PCurve<float,3>*                   prepareButterfly();
  GMlib::PCurve<float,3>*                   prepareXFigure(float radius=5.0f);
  // Surfaces
  GMlib::PSurf<float,3>*                    preparePlanet();
  GMlib::PSurf<float,3>*                    prepareCylinder();
  GMlib::PSurf<float,3>*                    prepareTorus();
  GMlib::PSurf<float,3>*                    preparePlane();
  GMlib::PSurf<float,3>*                    prepareDini();
  // GERBS
  GMlib::GERBSCurve<float>*                 prepareGERBSCurve(GMlib::PCurve<float,3>* &curve, int n, const GMlib::Color& color, GMlib::SimulationMode sm);
  GMlib::GERBSSurf<float>*                  prepareGERBSSurf(GMlib::PSurf<float,3>* &model, int n, int m, const GMlib::Material& material, GMlib::SurfaceShape shape);

  // Curves
  GMlib::PCurve<float,3>*                   _blend_curves;
  GMlib::PCurve<float,3>*                   _bspline_one;
  GMlib::PCurve<float,3>*                   _bspline_two;
  GMlib::PCurve<float,3>*                   _cylinderical_sine_wave;
  GMlib::PCurve<float,3>*                   _rose;
  GMlib::PCurve<float,3>*                   _clover;
  GMlib::PCurve<float,3>*                   _circle;
  GMlib::PCurve<float,3>*                   _sample_circle;
  GMlib::PCurve<float,3>*                   _reef;
  GMlib::PCurve<float,3>*                   _butterfly;
  GMlib::PCurve<float,3>*                   _xfigure;
  GMlib::PCurve<float,3>*                   _asteroid;
  std::vector<GMlib::GERBSCurve<float>*>    _gerbs_curve;
  // Surfaces
  GMlib::PSurf<float,3>*                    _planet;
  GMlib::PSurf<float,3>*                    _cylinder;
  GMlib::PSurf<float,3>*                    _torus;
  GMlib::PSurf<float,3>*                    _plane;
  GMlib::PSurf<float,3>*                    _dini;
  std::vector<GMlib::GERBSSurf<float>*>     _gerbs_surface;

  int                                       _max = 5;
  int                                       _sim_counter = 0;
};



#endif // SCENARIO_H
