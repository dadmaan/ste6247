#ifndef GM_PARAMETRICS_CURVES_BSPLINE_H
#define GM_PARAMETRICS_CURVES_BSPLINE_H

// gmlib
#include <parametrics/gmpcurve.h>
namespace GMlib{

    template <typename T>
    class BSpline : public PCurve<T,3> {
        GM_SCENEOBJECT(BSpline)
public:
    BSpline(const DVector<Vector<T,3>>& c);
    BSpline(const DVector<Vector<T,3>> p, int n);

    //Virtual public function from PCurve
    bool            isClosed() const override;

protected:
    //Virtual protected functions from PCurve
    void            eval(T t, int d, bool l)const override;
    T               getStartP() const override;
    T               getEndP() const override;
private:
    int             findI(T t) const;
    float           getW(T t, int d, int i) const;
    void            generateKnots();
    void            generateControlPoints();
    void            generateAMatrix();
    Vector<T,3>     getBasis(T t, int& i) const;
    T               getDt() const;
    //Intrinsic data for curve
    DMatrix<T>              A;
    DVector<Vector<T,3>>    _c;
    DVector<Vector<T,3>>    _point;
    T                       _d,_k,_m,_n;
    std::vector<T>          _t;

};   //End class BSpline

//Include BSpline class function implementation

//*****************************
//Default constructor
//*****************************
template <typename T>
inline
BSpline<T>::BSpline(const DVector<Vector<T,3> >& c):PCurve<T,3>(20,0,0){
    _n = c.getDim();
    _c = c;
    _d = 2;
    _k = _d+1;
    generateKnots();
    }

//*****************************
//Second constructor
//*****************************
template <typename T>
inline
BSpline<T>::BSpline(DVector<Vector<T,3>> p, int n):PCurve<T,3>(20,0,0){
    _n = n;
    _d = 2;
    _k = _d+1;
    _m = p.getDim();
    _point = p;
    generateKnots();
    generateAMatrix();
    generateControlPoints();
    }
//*****************************
//This curve is cyclical
//*****************************
template <typename T>
bool  BSpline<T>::isClosed() const {return true;}

//****************************************
//Implementation of formula for the curve
//****************************************
template<typename T>
void BSpline<T>::eval(T t, int d, bool /*l*/) const {
    this->_p.setDim(d+1);
    int i;
    auto b = getBasis(t,i);
    this->_p[0] = b[0]*_c[i-2] + b[1]*_c[i-1] + b[2]*_c[i];
}

//****************************************
//Functions
//****************************************
template<typename T>
float BSpline<T>::getW(T t, int d, int i) const {
    return t - _t[i]/ _t[i+d] - _t[i];
}

template<typename T>
int BSpline<T>::findI(T t) const {
    int i = 0;
    for (i = _d; i < static_cast<int>(_t.size()); i++ )
      if (_t[i] > t)
        break;

    if (i > _n)
      i = _n;
    i--; // Have to reduce i, since the index is point to greater element.
    return i;
}

template<typename T>
void BSpline<T>::generateKnots() {

    _t.resize(_n+_k);
    for (int i=0; i<_k; i++)        // Generate  0k number of zero
        _t[i] = 0;
    for (int i=_k; i<=_n; i++)      // Generate k-n number of 1,2,..,x
        _t[i] = i-_d;
    for (int i=_n+1; i<_n+_k; i++)  // Generate  k number of x
        _t[i] = _t[i-1];
}

template<typename T>
void BSpline<T>::generateControlPoints() {
        //******************************
        //* Ax=b -> x=(A^t*A)^-1*A^t*b *
        //******************************
    auto A_transpose = A;
    A_transpose.transpose();
    auto AtA = A_transpose*A;       // A^t*A
    auto b = A_transpose*_point;    // A^t*b
    _c = AtA.invert()*b;            // x=(A^t*A)^-1*A^t*b
}

template<typename T>
void BSpline<T>::generateAMatrix() {
    A = GMlib::DMatrix<T>(_m, _n, T(0));

    const T dt = this->getDt();
    GMlib::Vector<T,3> basis;
    int i;
    for(int k =0; k < _m; k++)
    {
      basis = getBasis(_t[_d] + k*dt, i);
      for (int l = i-_d; l <= i; l++)
      {
        //std::cout << "[k]: " << k << " [l]: " << l << std::endl;
        A[k][l] = basis[l-i+_d];
      }
    }
}

template<typename T>
Vector<T,3> BSpline<T>::getBasis(T t, int &i) const {
    GMlib::Vector<T, 3> b;
    DMatrix<T> A(1,2);
    DMatrix<T> B(2,3);

    i = findI(t);
    A[0][0] = 1-getW(t,1,i);
    A[0][1] = getW(t,1,i);

    B[0][0] = 1-getW(t,2,i-1);
    B[0][1] = getW(t,2,i-1);
    B[0][2] = 0;

    B[1][0] = 0;
    B[1][1] = 1-getW(t,2,i);
    B[1][2] = getW(t,2,i);

    b[0] = A[0][0]*B[0][0];
    b[1] = A[0][0]*B[0][1] + A[0][1]*B[1][1];
    b[2] = A[0][1]*B[1][2];
    return b;
}

template<typename T>
T BSpline<T>::getDt() const {
    return (_t[_n] - _t[_d])/(_m - 1);
}
//****************************************************
//The start and end of the defined domain (intervall)
//****************************************************
template<typename T>
T BSpline<T>::getStartP() const {
    return _t[_d];
}
template<typename T>
T BSpline<T>::getEndP() const {
    return _t[_n];
    }
#endif //GM_PARAMETRICS_CURVES_PBUTTERFLY_H
}
