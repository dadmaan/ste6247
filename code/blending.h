#ifndef GM_PARAMETRICS_CURVES_PBLEND_H
#define GM_PARAMETRICS_CURVES_PBLEND_H

// gmlib
#include <parametrics/gmpcurve.h>
namespace GMlib{

    template <typename T>
    class PBlend : public PCurve<T,3> {
        GM_SCENEOBJECT(PBlend)
public:
    PBlend(PCurve<float,3>* c1, PCurve<float,3>* c2, T t);
    virtual ~PBlend();

    void                    checkForUpdates();

protected:
    //Virtual protected functions from PCurve
    void                    eval(T t, int d, bool l)const override;
    void                    localSimulate(double dt) override;
    T                       getStartP() const override;
    T                       getEndP() const override;

private:
    T                       trigonometricFunction(T t)  const;
    T                       polynomialFunction(T t)     const;
    //Intrinsic data for curve
    PCurve<float,3>*        _curve1;
    PCurve<float,3>*        _curve2;
    T                       t_blend;

};   //End class Blending

//Include Blending class function implementation

//*****************************
//Constructor
//*****************************
template <typename T>
inline
PBlend<T>::PBlend(PCurve<float,3>* c1, PCurve<float,3>* c2, T t){
    _curve1 = c1;
    _curve2 = c2;
    t_blend = t;
    _curve2->setDomain(_curve1->getParStart() + (1-t_blend) * _curve1->getParDelta(),//s1+0.8*delta1
                       _curve1->getParStart() + (2-t_blend) * _curve1->getParDelta());
}

//*****************************
//Destructor
//*****************************
template <typename T>
PBlend<T>::~PBlend() {}

//****************************************
//Implementation of formula for the curve
//****************************************
template<typename T>
void PBlend<T>::eval(T t, int d, bool /*l*/) const {
    this->_p.setDim(d+1);

    auto domain_2 = _curve1->getParEnd();
    auto domain_1 = domain_2 - t_blend*_curve1->getParDelta();
    if(t<=domain_1)                             //First Domain
        this->_p[0] = _curve1->evaluateParent(t,0)[0];

    if(domain_1<t && t<domain_2){              //Second Domain
        T b =  trigonometricFunction((t - _curve2->getParStart())/(_curve1->getParEnd() - _curve2->getParStart()));
        Vector<T,3> p1 = _curve1->evaluateParent(t,0)[0];
        Vector<T,3> p2 = _curve2->evaluateParent(t,0)[0];
        this->_p[0] = (p1 + b * (p2 - p1)) ;

    }

    if(domain_2<=t)                             //Third Domain
        this->_p[0] = _curve2->evaluateParent(t,0)[0];
}

template<typename T>
void PBlend<T>::localSimulate(double /*dt*/){this->sample(100,0);}
//****************************************
//Functions
//****************************************
template<typename T>
T PBlend<T>::trigonometricFunction(T t) const{
    //***********************
    //* B(t)= sin^2(PI/2*t) *
    //***********************
    T s =sin(M_PI/2*t);
    return s*s;
}

template<typename T>
T PBlend<T>::polynomialFunction(T t) const{
    //*********************
    //* B(t)= 3t^2 - 2t^3 *
    //*********************
    return 3*t*t - 2*t*t*t;
}

template<typename T>
void PBlend<T>::checkForUpdates(){
    if (_curve1->getEditDone() || _curve2->getEditDone())
    {
      std::cout << "blending curve's checking for updates is done..." << std::endl;
      this->sample(100,4);
      this->replot();
    }
}

//****************************************************
//The start and end of the defined domain (intervall)
//****************************************************
template<typename T>
T PBlend<T>::getStartP() const {
    return _curve1->getParStart();
}
template<typename T>
T PBlend<T>::getEndP() const {
    return _curve2->getParEnd();
    }
#endif //GM_PARAMETRICS_CURVES_PBLEND_H
}
