#ifndef GM_PARAMETRICS_CURVES_CYLINDERICALSINEWAVE_H
#define GM_PARAMETRICS_CURVES_CYLINDERICALSINEWAVE_H

// gmlib
#include <parametrics/gmpcurve.h>
namespace GMlib{

    template <typename T>
    class CylindericalSineWave : public PCurve<T,3> {
        GM_SCENEOBJECT(CylindericalSineWave)
public:
    CylindericalSineWave(T size = T(5) );
    CylindericalSineWave(const CylindericalSineWave<T>& copy);

    //Virtual public function from PCurve
    bool                isClosed() const override;

protected:
    //Virtual protected functions from PCurve
    void                eval(T t, int d, bool l)const override;
    T                   getStartP() const override;
    T                   getEndP() const override;
    //Intrinsic data for curve
    T                   _size;

};   //End class CylindericalSineWave





//*****************************
//Default constructor
//*****************************

template <typename T>
inline
CylindericalSineWave<T>::CylindericalSineWave(T size):PCurve<T,3>(20,0,0), _size(size){}

//*****************************
//Copy constructor
//*****************************
template <typename T>
inline
CylindericalSineWave<T>::CylindericalSineWave(const CylindericalSineWave<T>& copy):PCurve<T,3>(copy), _size(copy._size){}

//*****************************
//This curve is cyclical
//*****************************
template <typename T>
bool  CylindericalSineWave<T>::isClosed() const {return true;}

//****************************************
//Implementation of formula for the curve
//****************************************
template<typename T>
void CylindericalSineWave<T>::eval(T t, int d, bool /*l*/) const {
    this->_p.setDim(d+1);

    T n = 8;
    T a = 5;
    T b = 2;
    this->_p[0][0] = a*cos(t);
    this->_p[0][1] = a*sin(t);
    this->_p[0][2] = b*cos(n*t);

}
//****************************************************
//The start and end of the defined domain (intervall)
//****************************************************
    template<typename T>
    T CylindericalSineWave<T>::getStartP() const {
        return T(0);
    }
    template<typename T>
    T CylindericalSineWave<T>::getEndP() const {
        return T(2 * M_PI);
    }
#endif //GM_PARAMETRICS_CURVES_CYLINDERICALSINEWAVE_H
}
