#ifndef GM_PARAMETRICS_CURVES_PROSE_H
#define GM_PARAMETRICS_CURVES_PROSE_H

// gmlib
#include <parametrics/gmpcurve.h>
namespace GMlib{

    template <typename T>
    class PRose : public PCurve<T,3> {
        GM_SCENEOBJECT(PRose)
public:
    PRose(T size = T(5) );
    PRose(const PRose<T>& copy);

    //Virtual public function from PCurve
    bool  isClosed() const override;

protected:
    //Virtual protected functions from PCurve
    void    eval(T t, int d, bool l)const override;
    T       getStartP() const override;
    T       getEndP() const override;
    //Intrinsic data for curve
    T       _size;
};   //End class Rose

//Include PRose class function implementation

//*****************************
//Default constructor
//*****************************

template <typename T>
inline
PRose<T>::PRose(T size):PCurve<T,3>(20,0,0), _size(size){
    //Note that the last parameter in the PCurve constructor is 2,
    // this because 2 derivatives in eval() is implemented
    }

//*****************************
//Copy constructor
//*****************************
template <typename T>
inline
PRose<T>::PRose(const PRose<T>& copy):PCurve<T,3>(copy), _size(copy._size){}

//*****************************
//This curve is cyclical
//*****************************
template <typename T>
bool  PRose<T>::isClosed() const {return true;}

//****************************************
//Implementation of formula for the curve
//****************************************
template<typename T>
void PRose<T>::eval(T t, int d, bool /*l*/) const {
    this->_p.setDim(d+1);

    //Rose Curve
    auto a = 15/9;
    auto b = 8;
    this->_p[0][0] = cos(t) * (a + cos(b*t));
    this->_p[0][1] = sin(t) * (a + cos(b*t));
    this->_p[0][2] = 0;

}
//****************************************************
//The start and end of the defined domain (intervall)
//****************************************************
    template<typename T>
    T PRose<T>::getStartP() const {
        return T(0);
    }
    template<typename T>
    T PRose<T>::getEndP() const {
        return T(2 * M_PI);
    }
#endif //GM_PARAMETRICS_CURVES_PCLOVER_H
}
