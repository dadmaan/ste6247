#ifndef GM_PARAMETRICS_CURVES_XFIGURE_H
#define GM_PARAMETRICS_CURVES_XFIGURE_H

// gmlib
#include <parametrics/gmpcurve.h>



namespace GMlib{




    template <typename T>
    class XFigure : public PCurve<T,3> {
        GM_SCENEOBJECT(XFigure)

public:
    XFigure(T radius = T(10));
    XFigure(const XFigure<T>& copy);

    //Virtual public function from PCurve
    bool                    isClosed() const override;

protected:
    //Virtual protected functions from PCurve
    void                    eval(T t, int d, bool l)const override;
    T                       getStartP() const override;
    T                       getEndP() const override;
    T                       getRadius() const;
private:

    //Intrinsic data for curve
    T                       _r;

};   //End class XFigure

//Include XFigure class function implementation

//*****************************
//Default constructor
//*****************************
template <typename T>
inline
XFigure<T>::XFigure(T radius):PCurve<T,3>(20,0,0){ _r = radius;}



//*****************************
//Second constructor
//*****************************
template <typename T>
inline
XFigure<T>::XFigure(const XFigure<T>& copy):PCurve<T,3>(copy){}



//*****************************
//This curve is cyclical
//*****************************
template <typename T>
bool  XFigure<T>::isClosed() const {return true;}



//****************************************
//Implementation of formula for the curve
//****************************************
template<typename T>
void XFigure<T>::eval(T t, int d, bool /*l*/) const {

    this->_p.setDim(d+1);

    T ct    = 0;
    T st    = 0;
    T n_1   = 10.0;
    T n_2   = 10.0;
    T n_3   = 10.0;
    T m     = 8;

    T t_1   = 2*cos((m*t)/4)/_r;
    t_1     = abs(t_1);
    t_1     = pow(t_1, n_2);

    T t_2   = sin((m*t)/4)/_r;
    t_2     = abs(t_2);
    t_2     = pow(t_2, n_3);


    T radius = pow(t_1+t_2,1/n_1);

    if(abs(radius) == 0) {
      ct = 0;
      st = 0;
    }
    else
    {
      radius = 1/radius;
      ct = radius*cos(t);
      st = radius*sin(t);
    }


    //this->_p[0] = b_0 * c_{i-3} + b1 * c_{i-2} + b_2 ...
    this->_p[0][0] = ct;
    this->_p[0][1] = st;
    this->_p[0][2] = T(0);
}

//****************************************
//Functions
//****************************************
template<typename T>
T XFigure<T>::getRadius() const { return _r;}

//****************************************************
//The start and end of the defined domain (intervall)
//****************************************************
template<typename T>
T XFigure<T>::getStartP() const { return T(0);}

template<typename T>
T XFigure<T>::getEndP() const { return T(M_2PI);}

#endif //GM_PARAMETRICS_CURVES_XFIGURE_H
}
