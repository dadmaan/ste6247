#ifndef GM_PARAMETRICS_CURVES_BSPLINE_H
#define GM_PARAMETRICS_CURVES_BSPLINE_H

// gmlib
#include <parametrics/gmpcurve.h>
//#include <QtDebug>
#include <scene/selector/gmselector.h>
#include <scene/visualizers/gmselectorgridvisualizer.h>


namespace GMlib{

//template <typename T, int n>
//class Selector;

//template <typename T>
//class SelectorGridVisualizer;





    template <typename T>
    class BSpline : public PCurve<T,3> {
        GM_SCENEOBJECT(BSpline)
private:
    struct EditSet {
        int         ind;            //!< Index of a selector and thus a control point
        Vector<T,3> dp;             //!< The distance the control point has been moved
        EditSet(int i, const Vector<T,3>& d): ind(i),dp(d){}
    };
public:
    BSpline(const DVector<Vector<T,3>>& c);
    BSpline(const DVector<Vector<T,3>> p, int n);
    BSpline(const BSpline<T>& copy);
    virtual ~BSpline();

    // Virtual public function from PCurve
    bool            isClosed() const override;
    // Selector functions
    void            showSelectors( T radius = T(1), bool grid = true,
                                   const Color& selector_color = GMcolor::darkBlue(),
                                   const Color& grid_color = GMcolor::lightGreen()) override;
    void            edit(int selector, const Vector<T,3>& dp) override;

protected:
    //Virtual protected functions from PCurve
    void                                eval(T t, int d, bool l)const override;
    void                                replot() const override;
    T                                   getStartP() const override;
    T                                   getEndP() const override;
    void                                localSimulate(double dt) override;
private:
    int                                 findI(T t) const;
    float                               getW(T t, int d, int i) const;
    void                                generateKnots();
    void                                generateControlPoints();
    void                                generateAMatrix();
    Vector<T,3>                         getBasis(T t, int& i) const;
    T                                   getDt() const;

    //Intrinsic data for curve
    DMatrix<T>                          A;          // A-Matrix for least square
    DVector<Vector<T,3>>                _c;         // Control points
    DVector<Vector<T,3>>                _point;     // Ponits
    T                                   _d;         // Curve degree
    T                                   _k;
    T                                   _m;
    T                                   _n;
    std::vector<T>                      _t;         // Knot vector

    // Variables
    SelectorGridVisualizer<T>*          _sgv;                   // Selectorgrid
    std::vector<Selector<T,3>*>         _s;                     // A set of selectors (spheres)
    GMlib::Color                        _selector_color;
    GMlib::Color                        _grid_color;
    T                                   _selector_radius;
    bool                                _update_sample_flag{false};
    bool                                _selectors_flag;             // Mark if we have selectors or not
    bool                                _grid_flag;                  // Mark if we have a selector grid or not
    bool                                _cl {false};                // closed (or open) curve?
    mutable bool                        _c_moved;                   // Mark that we are editing, moving controll points
    mutable std::vector<EditSet>        _pos_change;

};   //End class BSpline

//Include BSpline class function implementation

//*****************************
//Default constructor
//*****************************
template <typename T>
inline
BSpline<T>::BSpline(const DVector<Vector<T,3> >& c):PCurve<T,3>(20,0,0){
    _n = c.getDim();
    _c = c;
    _d = 2;
    _k = _d+1;
    generateKnots();
    }

//*****************************
//Second constructor
//*****************************
template <typename T>
inline
BSpline<T>::BSpline(DVector<Vector<T,3>> p, int n):PCurve<T,3>(20,0,0){
    _n = n;
    _d = 2;
    _k = _d+1;
    _m = p.getDim();
    _point = p;

    generateKnots();
    generateAMatrix();
    generateControlPoints();
    }

//*****************************
// Third constructor
//*****************************
template <typename T>
inline
BSpline<T>::BSpline(const BSpline<T>& copy):PCurve<T,3>(copy){}

//*****************************
// Destructor
//*****************************
template <typename T>
BSpline<T>::~BSpline(){}

//*****************************
// This curve is cyclical
//*****************************
template <typename T>
bool  BSpline<T>::isClosed() const {return false;}

//****************************************
// Implementation of formula for the curve
//****************************************
template<typename T>
void BSpline<T>::eval(T t, int d, bool /*l*/) const {

    this->_p.setDim(d+1);

    int i;
    auto b = getBasis(t,i);

    this->_p[0] = b[0]*_c[i-2]
                + b[1]*_c[i-1]
                + b[2]*_c[i];
}

//****************************************
//Local simulate function
//****************************************
template<typename T>
void BSpline<T>::localSimulate(double /*dt*/) {
    if(_update_sample_flag){this->sample(100, 0);_update_sample_flag = false;}
}

//****************************************
// Implementation of selectors
//****************************************
template<typename T>
void BSpline<T>::showSelectors(T rad, bool grid, const Color& sc, const Color& gc){
    if(!_selectors_flag){
        _s.resize(_c.getDim());

        for( int i = 0; i < _c.getDim(); i++ )
            if(this->isScaled()){
                this->insert(_s[i] = new Selector<T,3>(_c[i],
                                                       i,
                                                       this,
                                                       this->_scale.getScale(),
                                                       rad,
                                                       sc));
            }
            else{
                this->insert(_s[i] = new Selector<T,3>(_c[i],
                                                       i,
                                                       this,
                                                       rad,
                                                       sc));
            }

        _selectors_flag = true;
    }

    _selector_radius = rad;
    _selector_color  = sc;

    if(grid){
        if(!_sgv) _sgv = new SelectorGridVisualizer<float>();
        _sgv->setSelectors(_c,0,_cl);
        _sgv->setColor(gc);
        this->SceneObject::insertVisualizer(_sgv);
        this->setEditDone();
    }
    _grid_color     = gc;
    _grid_flag      = grid;
}
template<typename T>
void BSpline<T>::replot() const{ PCurve<T,3>::replot();}

template<typename T>
void BSpline<T>::edit(int /*selector_id*/, const Vector<T,3>& /*dp*/){

    _c_moved = true;
       if( this->_parent ) this->_parent->edit( this );
       if( this->_derived ) this->_derived->edit( this );
        _update_sample_flag = true;
       this->setEditDone();
    _c_moved = false;

}

//****************************************
// BSpline Functions
//****************************************
template<typename T>
float BSpline<T>::getW(T t, int d, int i) const {
    return ((t - _t[i])/ (_t[i+d] - _t[i]));
}

template<typename T>
int BSpline<T>::findI(T t) const {

    int i = 0;
    for (i = _d; i < _n; i++ )
      if (_t[i] <= t && t < _t[i+1])
        break;

    if (i >= _n)
      i = _n-1;
    return i;
}

template<typename T>
void BSpline<T>::generateKnots() {

    _t.resize(_n+_k);
    for (int i=0; i<_k; i++)        // Generate  0k number of zero
        _t[i] = 0;
    for (int i=_k; i<=_n; i++)      // Generate k-n number of 1,2,..,x
        _t[i] = i-_d;
    for (int i=_n+1; i<_n+_k; i++)  // Generate  k number of x
        _t[i] = _t[i-1];
}

template<typename T>
void BSpline<T>::generateControlPoints() {
    //******************************
    //* Ax=b -> x=(A^t*A)^-1*A^t*b *
    //******************************
    auto A_transpose = A;
    A_transpose.transpose();
    auto AtA = A_transpose*A;       // A^t*A
    auto b = A_transpose*_point;    // A^t*b
    _c = AtA.invert()*b;            // x=(A^t*A)^-1*A^t*b
}

template<typename T>
void BSpline<T>::generateAMatrix() {

    A = GMlib::DMatrix<T>(_m, _n, T(0));

    const T dt = this->getDt();
    GMlib::Vector<T,3> basis;

    int i;
    for(int k =0; k < _m; k++){

      basis = getBasis(_t[_d] + k*dt, i);
      for (int l = i-_d; l <= i; l++)
        A[k][l] = basis[l-i+_d];

    }
}

template<typename T>
Vector<T,3> BSpline<T>::getBasis(T t, int &i) const {
    GMlib::Vector<T, 3> b;
    DMatrix<T> A(1,2);
    DMatrix<T> B(2,3);

    i = findI(t);
    A[0][0] = 1-getW(t,1,i);
    A[0][1] = getW(t,1,i);

    B[0][0] = 1-getW(t,2,i-1);
    B[0][1] = getW(t,2,i-1);
    B[0][2] = 0;

    B[1][0] = 0;
    B[1][1] = 1-getW(t,2,i);
    B[1][2] = getW(t,2,i);

    b[0] = A[0][0]*B[0][0];
    b[1] = A[0][0]*B[0][1] + A[0][1]*B[1][1];
    b[2] = A[0][1]*B[1][2];
    return b;
}

template<typename T>
T BSpline<T>::getDt() const {
    return (_t[_n] - _t[_d])/(_m - 1);
}
//****************************************************
//The start and end of the defined domain (intervall)
//****************************************************
template<typename T>
T BSpline<T>::getStartP() const {
    return _t[_d];
}
template<typename T>
T BSpline<T>::getEndP() const {
    return _t[_n];
    }

#endif //GM_PARAMETRICS_CURVES_BSPLINE_H
}
