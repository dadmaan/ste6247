#ifndef GM_PARAMETRICS_CURVES_PREEF_H
#define GM_PARAMETRICS_CURVES_PREEF_H

// gmlib
#include <parametrics/gmpcurve.h>
namespace GMlib{

    template <typename T>
    class PReef : public PCurve<T,3> {
        GM_SCENEOBJECT(PReef)
public:
    PReef(T size = T(5) );
    PReef(const PReef<T>& copy);

    //Virtual public function from PCurve
    bool  isClosed() const override;

protected:
    //Virtual protected functions from PCurve
    void    eval(T t, int d, bool l)const override;
    T       getStartP() const override;
    T       getEndP() const override;
    //Intrinsic data for curve
    T       _size;
};   //End class Reef

//Include PReef class function implementation

//*****************************
//Default constructor
//*****************************

template <typename T>
inline
PReef<T>::PReef(T size):PCurve<T,3>(20,0,0), _size(size){
    //Note that the last parameter in the PCurve constructor is 2,
    // this because 2 derivatives in eval() is implemented
    }

//*****************************
//Copy constructor
//*****************************
template <typename T>
inline
PReef<T>::PReef(const PReef<T>& copy):PCurve<T,3>(copy), _size(copy._size){}

//*****************************
//This curve is cyclical
//*****************************
template <typename T>
bool  PReef<T>::isClosed() const {return true;}

//****************************************
//Implementation of formula for the curve
//****************************************
template<typename T>
void PReef<T>::eval(T t, int d, bool /*l*/) const {
    this->_p.setDim(d+1);
    const double ct = cos(t);
    const double st = sin(t);

    //The position: c(t)
    this->_p[0][0] = _size*T(3*st + 2*sin(3*t));
    this->_p[0][1] = _size*T(ct - 2*cos(3*t));
    this->_p[0][2] = 2*cos(5*t);


}
//****************************************************
//The start and end of the defined domain (intervall)
//****************************************************
    template<typename T>
    T PReef<T>::getStartP() const {
        return T(0);
    }
    template<typename T>
    T PReef<T>::getEndP() const {
        return T(24.0 * M_PI);
    }
#endif //GM_PARAMETRICS_CURVES_PCLOVER_H
}
