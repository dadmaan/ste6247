#ifndef GM_PARAMETRICS_CURVES_PASTEROID_H
#define GM_PARAMETRICS_CURVES_PASTEROID_H

// gmlib
#include <parametrics/gmpcurve.h>
namespace GMlib{

    template <typename T>
    class PAsteroid : public PCurve<T,3> {
        GM_SCENEOBJECT(PAsteroid)
public:
    PAsteroid(T size = T(5) );
    PAsteroid(const PAsteroid<T>& copy);

    //Virtual public function from PCurve
    bool                isClosed() const override;

protected:
    //Virtual protected functions from PCurve
    void                eval(T t, int d, bool l)const override;
    T                   getStartP() const override;
    T                   getEndP() const override;
    //Intrinsic data for curve
    T                   _size;
};   //End class Asteroid

//Include PAsteroid class function implementation

//*****************************
//Default constructor
//*****************************

template <typename T>
inline
PAsteroid<T>::PAsteroid(T size):PCurve<T,3>(20,0,0), _size(size){
    //Note that the last parameter in the PCurve constructor is 2,
    // this because 2 derivatives in eval() is implemented
    }

//*****************************
//Copy constructor
//*****************************
template <typename T>
inline
PAsteroid<T>::PAsteroid(const PAsteroid<T>& copy):PCurve<T,3>(copy), _size(copy._size){}

//*****************************
//This curve is cyclical
//*****************************
template <typename T>
bool  PAsteroid<T>::isClosed() const {return true;}

//****************************************
//Implementation of formula for the curve
//****************************************
template<typename T>
void PAsteroid<T>::eval(T t, int d, bool /*l*/) const {
    this->_p.setDim(d+1);
    const double ct = cos(t);
    const double st = sin(t);

    //Astroid
    this->_p[0][0] = 5*ct*ct*ct;
    this->_p[0][1] = 5*st*st*st;
    this->_p[0][2] = 0;
}
//****************************************************
//The start and end of the defined domain (intervall)
//****************************************************
    template<typename T>
    T PAsteroid<T>::getStartP() const {
        return T(0);
    }
    template<typename T>
    T PAsteroid<T>::getEndP() const {
        return T(2 * M_PI);
    }
#endif //GM_PARAMETRICS_CURVES_PASTEROID_H
}
