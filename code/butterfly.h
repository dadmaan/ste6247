#ifndef GM_PARAMETRICS_CURVES_PBUTTERFLY_H
#define GM_PARAMETRICS_CURVES_PBUTTERFLY_H

// gmlib
#include <parametrics/gmpcurve.h>
namespace GMlib{

    template <typename T>
    class PButterfly : public PCurve<T,3> {
        GM_SCENEOBJECT(PButterfly)
public:
    PButterfly(T size = T(5) );
    PButterfly(const PButterfly<T>& copy);

    //Virtual public function from PCurve
    bool            isClosed() const override;

protected:
    //Virtual protected functions from PCurve
    void            eval(T t, int d, bool l)const override;
    T               getStartP() const override;
    T               getEndP() const override;
    //Intrinsic data for curve
    T               _size;
};   //End class Butterfly

//Include PButterfly class function implementation

//*****************************
//Default constructor
//*****************************

template <typename T>
inline
PButterfly<T>::PButterfly(T size):PCurve<T,3>(20,0,0), _size(size){
    //Note that the last parameter in the PCurve constructor is 2,
    // this because 2 derivatives in eval() is implemented
    }

//*****************************
//Copy constructor
//*****************************
template <typename T>
inline
PButterfly<T>::PButterfly(const PButterfly<T>& copy):PCurve<T,3>(copy), _size(copy._size){}

//*****************************
//This curve is cyclical
//*****************************
template <typename T>
bool  PButterfly<T>::isClosed() const {return true;}

//****************************************
//Implementation of formula for the curve
//****************************************
template<typename T>
void PButterfly<T>::eval(T t, int d, bool /*l*/) const {
    this->_p.setDim(d+1);
    const double ct = cos(t);
    const double st = sin(t);
    const double ct2 = cos(t/12);
    const double st2 = sin(t/12);
    const double a = exp(ct)-2*cos(4*t)-pow(st2,5);
    const double a1 = -exp(ct)*st*st - exp(ct)*ct+8*4*cos(4*t);
    const double a2 = exp(ct)*st*st - exp(ct)*ct* + 8*4*cos(4*t)
                      - 5*(pow(st2,3)*pow(ct2,2)/3 - pow(st2,5)/12)/12;
    //The position: c(t)
    this->_p[0][0] = _size*T(ct*a);
    this->_p[0][1] = _size*T(st*a);
    this->_p[0][2] = fabs(this->_p[0][1])/2;

    if(this->_dm == GM_DERIVATION_EXPLICIT){
        if(d>0){
            this->_p[1][0] = _size*T(-st*a + ct*a1);
            this->_p[1][1] = _size*T(ct*a + st*a1);
            this->_p[1][2] = fabs(this->_p[1][1])/2;
        }
        if(d>1){
            this->_p[2][0] = _size*T(-ct*a - st*a1 - st*a1 + ct*a2);
            this->_p[2][1] = _size*T(-st*a + ct*a1 + ct*a1 + st*a2);
            this->_p[2][2] = fabs(this->_p[2][1])/2;
        }
    }
}
//****************************************************
//The start and end of the defined domain (intervall)
//****************************************************
    template<typename T>
    T PButterfly<T>::getStartP() const {
        return T(0);
    }
    template<typename T>
    T PButterfly<T>::getEndP() const {
        return T(24.0 * M_PI);
    }
#endif //GM_PARAMETRICS_CURVES_PBUTTERFLY_H
}
