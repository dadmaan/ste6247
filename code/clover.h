#ifndef GM_PARAMETRICS_CURVES_PCLOVER_H
#define GM_PARAMETRICS_CURVES_PCLOVER_H

// gmlib
#include <parametrics/gmpcurve.h>
namespace GMlib{

    template <typename T>
    class PClover : public PCurve<T,3> {
        GM_SCENEOBJECT(PClover)
public:
    PClover(T size = T(5) );
    PClover(const PClover<T>& copy);

    //Virtual public function from PCurve
    bool  isClosed() const override;

protected:
    //Virtual protected functions from PCurve
    void    eval(T t, int d, bool l)const override;
    T       getStartP() const override;
    T       getEndP() const override;
    //Intrinsic data for curve
    T       _size;
};   //End class Butterfly

//Include PButterfly class function implementation

//*****************************
//Default constructor
//*****************************

template <typename T>
inline
PClover<T>::PClover(T size):PCurve<T,3>(20,0,0), _size(size){
    //Note that the last parameter in the PCurve constructor is 2,
    // this because 2 derivatives in eval() is implemented
    }

//*****************************
//Copy constructor
//*****************************
template <typename T>
inline
PClover<T>::PClover(const PClover<T>& copy):PCurve<T,3>(copy), _size(copy._size){}

//*****************************
//This curve is cyclical
//*****************************
template <typename T>
bool  PClover<T>::isClosed() const {return true;}

//****************************************
//Implementation of formula for the curve
//****************************************
template<typename T>
void PClover<T>::eval(T t, int d, bool /*l*/) const {
    this->_p.setDim(d+1);
//    const double ct = cos(t);
//    const double st = sin(t);

    //Spiral
//    this->_p[0][0] = (1-t)*ct;
//    this->_p[0][1] = 2*t;
//    this->_p[0][2] = (1-t)*st;


    // Cardoid Curve
    this->_p[0][0] = cos(t)*cos((7/4)*t);
    this->_p[0][1] = sin(t)*cos((7/4)*t);
    this->_p[0][2] = 0;


    //Astroid
    //for [0,2PI]
//    this->_p[0][0] = 5*ct*ct*ct;
//    this->_p[0][1] = 5*st*st*st;
//    this->_p[0][2] = 0;

    //Some shape
    //for [0,24PI]
//    this->_p[0][0] = t-(1.6*cos(24*t));
//    this->_p[0][1] = t-(1.6*sin(25*t));
//    this->_p[0][2] = t;

    //Lissajous Curve
    //for[0,12PI]
//    T a = 12;
//    T b = 13;
//    this->_p[0][0] = 4*sin((a/b)*t);
//    this->_p[0][1] = 3*sin(t);
    //    this->_p[0][2] = t;

    //Lissajous Curve 3D
    //for[0,12PI]
//    T m = 12;
//    T n = 8;
//    T a = 5;
//    T b = 2;
//    this->_p[0][0] = a*cos(t);
//    this->_p[0][1] = a*sin(t);
//    this->_p[0][2] = b*cos(n*t);

    //Clover
//    this->_p[0][0] = _size*T(cos(t) + 2*cos(2*t));
//    this->_p[0][1] = _size*T(sin(t) - 2*sin(2*t));
//    this->_p[0][2] = 2*sin(3*t);


    //Epicycloid Curve
//    auto a = 2;//fixed circle radius
//    auto b = 3;//circle radius which cycle around a
//    auto c = 1;//circle radius which cycle around a
//    this->_p[0][0] = (a-b)*cos(t) + c*cos(((a/b)-1)*t);
//    this->_p[0][1] = (a-b)*sin(t) - c*sin(((a/b)-1)*t);
//    this->_p[0][2] = 0;

    //Epicycloid Curve - [0,2PI]
//    this->_p[0][0] = cos(t) - cos(80*t) * sin(t);
//    this->_p[0][1] = 2*sin(t) - sin(80*t);
//    this->_p[0][2] = 2*t;

}
//****************************************************
//The start and end of the defined domain (intervall)
//****************************************************
    template<typename T>
    T PClover<T>::getStartP() const {
        return T(0);
    }
    template<typename T>
    T PClover<T>::getEndP() const {
        return T(2*M_PI);
    }
#endif //GM_PARAMETRICS_CURVES_PCLOVER_H
}
