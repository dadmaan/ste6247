#ifndef GM_PARAMETRICS_CURVES_GERBSCURVE_H
#define GM_PARAMETRICS_CURVES_GERBSCURVE_H

// gmlib
#include <parametrics/gmpcurve.h>
#include <parametrics/curves/gmpsubcurve.h>
#include "parametrics/surfaces/gmpsphere.h"

#include <random>
#include <memory>
namespace GMlib{


    enum class SimulationMode {
        None,
        Sureal,
        Orbit,
        Expand,
        Asteroid,
        Rose
    };


    template <typename T>
    class GERBSCurve : public PCurve<T,3> {
        GM_SCENEOBJECT(GERBSCurve)
public:
    GERBSCurve( PCurve<T,3>* curve, int n, SimulationMode sm);
    GERBSCurve(const GERBSCurve<T>& copy);

    //Virtual public function from PCurve
    bool                                    isClosed() const override;

protected:
    //Virtual protected functions from PCurve
    void                                    eval(T t, int d, bool l)const override;
    T                                       getStartP() const override;
    T                                       getEndP() const override;
    void                                    localSimulate(double dt) override;
private:
    T                                       trigonometricFunction(T t)  const;
    int                                     findI(T t) const;
    float                                   getW(T t, int d, int i) const;
    void                                    generateKnots();
    void                                    prepareLocalCurves();
    T                                       getDt() const;

    //Intrinsic data for curve
    DVector<Vector<T,3>>                    _c;             // Control points
    DVector<Vector<T,3>>                    _point;         // Points on the curve
    T                                       _d;             // Degree
    T                                       _k;             // Order
    T                                       _m;
    T                                       _n;
    std::vector<T>                          _t;             // Knot Vector
    // Container for curves
    PCurve<T,3>*                            _curve;         // Model curve
    DVector<PCurve<T,3>*>                   _local_curve;   // Container of Sub-curves

    // Simulation functions
private:
    void                                    createTranslationVectors();
    void                                    createRotationVectors();
    void                                    surealAnimation(const double& dt);
    void                                    rotationAnimation(const double& dt);
    void                                    orbitAnimation(const double& dt);
    void                                    expandAnimation(const double& dt);
    void                                    asteroidAnimation(const double& dt);
    void                                    roseAnimation(const double& dt);

    // Simulation variables
    GMlib::Color                            _color;
    double                                  _red   = 0;
    double                                  _blue  = 0;
    double                                  _green = 0;
    double                                  _alpha = 0;

    double                                  _accumulate;
    double                                  _counter = 0;
    double                                  _expand;// Expand
    double                                  _orbit;// Orbit
    bool                                    _up{true};

    std::uniform_real_distribution<double>  _step;
    std::mt19937                            _gen;

    SimulationMode                          _mode;      // Simulation mode
    std::vector<GMlib::Vector<T,3>>         _translate; // Container for translations
    std::vector<GMlib::Vector<T,3>>         _rotate;    // Container for Rotations

};   //End class GERBS

//Include GERBS class function implementation

//*****************************
//Default constructor
//*****************************
template <typename T>
inline
GERBSCurve<T>::GERBSCurve(PCurve<T,3>* curve, int n, SimulationMode sm):PCurve<T,3>(20,0,0){

    _curve = curve;
    _n = n;
    _d = 1;
    _k = _d+1;
    _mode = sm;

    if (isClosed())
        _n++;

    generateKnots();
    prepareLocalCurves();

    if(_mode == SimulationMode::Sureal)
    {
        createRotationVectors();
        createTranslationVectors();
    }
    else if(_mode == SimulationMode::Rose){
        _color = GMlib::Color(255,238,0);
        _color.toHSV();
        this->setColor(_color);
    }

    // Initializing random device
    std::random_device rd;
    _gen = std::mt19937(rd());
    _step = std::uniform_real_distribution<double>(0.0, 0.03);
}

//*****************************
//Second constructor
//*****************************
template <typename T>
inline
GERBSCurve<T>::GERBSCurve(const GERBSCurve<T>& copy):PCurve<T,3>(copy){}

//*****************************
//This curve is cyclical
//*****************************
template <typename T>
bool  GERBSCurve<T>::isClosed() const {return _curve->isClosed();}

//****************************************
//Implementation of formula for the curve
//****************************************
template<typename T>
void GERBSCurve<T>::eval(T t, int d, bool /*l*/) const {
    this->_p.setDim(d+1);

    auto i = findI(t);
    auto b = trigonometricFunction(getW(t,1,i));
    Vector<T,3> p1 = _local_curve[i-1]->evaluateParent(t,0)[0];
    Vector<T,3> p2 = _local_curve[i]->evaluateParent(t,0)[0];

    //*************************************************************
    //* C(t)= (1-B(w_1,i(t)) * C_i-1(t)) + (B(w_1,i(t)) * C_i(t)) *
    //*************************************************************
    this->_p[0] = (p1 * (1-b)) + (p2 * b);
}

//****************************************
//Local simulate function
//****************************************
template<typename T>
void GERBSCurve<T>::localSimulate(double dt) {

    _accumulate += dt;
//            rotationAnimation(dt);

    if (_mode == SimulationMode::Sureal)
        surealAnimation(dt);

    else if(_mode == SimulationMode::Orbit)
        orbitAnimation(dt);

    else if(_mode == SimulationMode::Expand)
        expandAnimation(dt);

    else if(_mode == SimulationMode::Asteroid)
        asteroidAnimation(dt);

    else if(_mode == SimulationMode::Rose){
        roseAnimation(dt);
//        colorAnimation(dt);
    }

    this->sample(300, 0);
}

//****************************************
//Functions
//****************************************
template<typename T>
T GERBSCurve<T>::trigonometricFunction(T t) const{
    //***********************
    //* B(t)= sin^2(PI/2*t) *
    //***********************
    T s =sin(M_PI/2*t);
    return s*s;
}

template<typename T>
float GERBSCurve<T>::getW(T t, int d, int i) const {
    return ((t - _t[i])/( _t[i+d] - _t[i]));
}

template<typename T>
int GERBSCurve<T>::findI(T t) const {

    int i = 0;
    for (i = _d; i < _n; i++ )
      if (_t[i] <= t && t < _t[i+1])
        break;

    if (i >= _n)
      i = _n-1;
//    i--; // Have to reduce i, since the index is point to greater element.
    return i;
}

template<typename T>
void GERBSCurve<T>::generateKnots() {

    _t.resize(_n+_k);

    _t[0] = _curve->getParStart();
    _t[1] = _t[0];
    T dt = (_curve->getParEnd() - _curve->getParStart())/T(_n-1);

    for (int i =2 ;i<_n;i++) {
        _t[i] = _t[1] + (T(i-1) * dt);
    }
    _t[_n] = _curve->getParEnd();
    _t[_n+1] = _t[_n];

    if(isClosed()){                 // Re-calculate the first and last knot
        _t[0]       = _t[1] - (_t[_n] - _t[_n-1]);
        _color.setAlpha(_alpha);
        _t[_n+1]    = _t[_n] + (_t[2] - _t[1]);
    }
//    for(unsigned int i =0; i<_t.size();i++)
std::cout<<"1st - "<<_t<<std::endl;
}

template<typename T>
void GERBSCurve<T>::prepareLocalCurves() {

    _local_curve.setDim(_n);
    for (int i =0; i < _n; i++)
    {
        if (isClosed() && i == _n-1)
            _local_curve[_n-1] = _local_curve[0];

        else{
            _local_curve[i] = new GMlib::PSubCurve<T>(_curve, _t[i], _t[i+_k], _t[i+_d]);
            _local_curve[i]->toggleDefaultVisualizer();
            _local_curve[i]->sample(10, 0);
            _local_curve[i]->setVisible(true);
            _local_curve[i]->setCollapsed(true);
            this->insert(_local_curve[i]);
        }

      if (i == 0)
        _local_curve[i]->setColor(GMlib::GMcolor::blue());

      else if (i == _n-_d)
        _local_curve[i]->setColor(GMlib::GMcolor::green());

      else
        _local_curve[i]->setColor(GMlib::GMcolor::purple());
    }

    if(_mode==SimulationMode::Orbit){
        auto moon = new GMlib::PSphere<float>(0.2f);
        moon->toggleDefaultVisualizer();
        moon->setMaterial(GMlib::GMmaterial::blackRubber());
        moon->sample(60,60,1,1);
        _local_curve[0]->insert(moon);
    }
}

template<typename T>
T GERBSCurve<T>::getDt() const {
    return (_t[_n] - _t[_d])/(_n - 1);
}
//****************************************************
//The start and end of the defined domain (intervall)
//****************************************************
template<typename T>
T GERBSCurve<T>::getStartP() const {
    return _t[_d];
}
template<typename T>
T GERBSCurve<T>::getEndP() const {
    return _t[_n];
    }

//****************************************
// Simulation Functions
//****************************************
template<typename T>
void GERBSCurve<T>::createTranslationVectors(){

    double angle;
    Vector<T,3> new_vec;
    //std::cout << "Main curve's matrix" << _mc->getMatrix() << std::endl;
    _translate.resize(_n);
    auto m = GMlib::SqMatrix<T, 2>();
    for (int i = 0; i < _n; ++i)
    {
      auto vec  = GMlib::Vector<T,2>(T(0.1), T(0));

      if(_mode == SimulationMode::Sureal)
          angle = 0;
      else
          angle = (M_PI/2)/_n*i;

      m[0][0]   = cos(angle);
      m[0][1]   = -sin(angle);
      m[1][0]   = sin(angle);
      m[1][1]   = cos(angle);

      vec       = m * vec;
      if(_mode == SimulationMode::Sureal)
        new_vec = GMlib::Vector<T,3>(vec[0]/10, T(0), T(0));
      else
        new_vec = GMlib::Vector<T,3>(vec[0], vec[1], T(0));

      _translate[i] = new_vec;
    }
}

template<typename T>
void GERBSCurve<T>::createRotationVectors(){

    _rotate.resize(_n);
    auto m = GMlib::SqMatrix<T, 2>();
    for (int i = 0; i < _n; ++i)
    {
      double angle  = (2*M_PI)/_n*i;
      m[0][0]       = cos(angle);
      m[0][1]       = -sin(angle);
      m[1][0]       = sin(angle);
      m[1][1]       = cos(angle);

      auto vec      = GMlib::Vector<T,2>(0.0f, 1.0f);
      vec           = m * vec;
      auto new_vec  = GMlib::Vector<T,3>(vec[0], vec[1], T(0));
      _rotate[i]    = new_vec;
      //std::cout << _rotation[i] << std::endl;
    }
}

template<typename T>
void GERBSCurve<T>::surealAnimation(const double& dt){

    for(int i = 0; i < _local_curve.getDim()-1; i++) {
      _local_curve[i]->rotate(dt, GMlib::Vector<T,3>(0.0f, 0.0f, 1.0f));

      if (i % 2 == 0)
        _local_curve[i]->translateGlobal(sin(_accumulate)*_translate[i]);
      else
        _local_curve[i]->translateGlobal(cos(_accumulate)*_translate[i]);
    }
}

template<typename T>
void GERBSCurve<T>::rotationAnimation(const double& dt){

    for(int i = 0; i < _local_curve.getDim()-1; i++) {
      if (i % 2 == 0)
        _local_curve[i]->rotate(dt, _rotate[i]);
      else
        _local_curve[i]->rotate(dt, -_rotate[i]);

      _local_curve[i]->translate(_translate[i]);
    }
}


template<typename T>
void GERBSCurve<T>::orbitAnimation(const double& dt){

    if (this->_counter <= 0.0) {
      this->_counter = 0.0;
      _up = true;
    }
    else if (this->_counter >= 2.0) {
      this->_counter = 2.0;
      _up = false;
    }

//    _orbit += _up ? + 0.055*dt : - 0.055*dt;
    this->_counter += _up ? _step(_gen) : -_step(_gen);
    auto angle = 1;

    float trans = float(dt*sin(angle*3*M_PI/2));

    _local_curve[0]->rotate(dt,Vector<float,3>(1,_up ? .5: 0 ,0));
    _local_curve[0]->translate({ _up ? -trans : trans, 0.0, 0.0 });
    _local_curve[1]->rotate(dt,Vector<float,3>(1,_up ? .5: 0 ,0));
    _local_curve[1]->translate({ _up ? trans : -trans, 0.0, 0.0 });

}

template<typename T>
void GERBSCurve<T>::expandAnimation( const double& dt){

    const Vector<float, 3> _rot1 = { 0.1f, 0.0f, 0.0f };

    if (this->_counter <= 0.0) {
      this->_counter = 0.0;
      _up = true;
    }
    else if (this->_counter >= 2.0) {
      this->_counter = 2.0;
      _up = false;
    }

    this->_counter += _up ? _step(_gen) : -_step(_gen);
    _alpha += _up ? 0.05 : -0.05;
    _color.setAlpha(_alpha);
    _color.toHSV();
    this->setColor(_color);

    _expand += _up ? 0.055 * dt : -0.055 * dt;

    float trans0    = _expand * 0.2f;
    float trans1    = _expand * 0.3f;
    float rot01     = _expand * 1.0f;

    // Translate subcurves
    //========================================================
    // X+-,Y+-,Z+-
    _local_curve[1]->translate({ _up ? trans1 : -trans1, _up ? trans1 : -trans1, _up ? trans1 : -trans1 });
    // X-+,Y+-,Z+-
    _local_curve[3]->translate({ _up ? -trans1 : trans1, _up ? trans1 : -trans1, _up ? trans1 : -trans1 });
    // X-+,Y-+,Z-+
    _local_curve[5]->translate({ _up ? -trans1 : trans1, _up ? -trans1 : trans1, _up ? trans1 : -trans1 });
    // X+-,Y-+,Z-+
    _local_curve[7]->translate({ _up ? trans1 : -trans1, _up ? -trans1 : trans1, _up ? -trans1 : trans1 });

    // to up
    _local_curve[0]->translate({ _up ? trans0 : -trans0 , 0.0f, 0.0f});
    // to left
    _local_curve[2]->translate({0.0, 0.0f,  _up ? trans0 : -trans0 });
    // to down
    _local_curve[4]->translate({ _up ? -trans0 : trans0 , 0.0f, 0.0f});
    // to right
    _local_curve[6]->translate({ 0.0, 0.0f,  _up ? -trans0 : trans0});
    //========================================================

    for (int j=0;j<_local_curve.getDim();j++)
        _local_curve[j]->rotate(_up ? -rot01 : rot01, _rot1);


}

template<typename T>
void GERBSCurve<T>::asteroidAnimation( const double& dt){


    if (this->_counter <= 0.0) {
      this->_counter = 0.0;
      _up = true;
    }
    else if (this->_counter >= 1.0) {
      this->_counter = 1.0;
      _up = false;
    }

    this->_counter += _up ? _step(_gen) : -_step(_gen);
    _expand += _up ? 0.055 * dt : -0.055 * dt;

    float trans0    = _expand * 0.3f;
    float trans1    = _expand * 0.3f;

    // Translate subcurves
    //========================================================
    // north
    _local_curve[0]->translate({ _up ? trans0 : -trans0, 0.0, 0.0 });
    // south
    _local_curve[2]->translate({ _up ? -trans0 : trans0, 0.0, 0.0 });
    // west
    _local_curve[1]->translate({0.0, 0.0, _up ? trans1 : -trans1 });
    // east
    _local_curve[3]->translate({ 0.0, 0.0, _up ? -trans1 : trans1 });

}

template<typename T>
void GERBSCurve<T>::roseAnimation( const double& dt){

    if (this->_counter <= 0.0) {
        this->_counter = 0.0;
        _up = true;
    }
    else if (this->_counter >= 1.0) {
        this->_counter = 1.0;
        _up = false;
    }

    this->_counter += _up ? _step(_gen) : -_step(_gen);
    _color.setGreen(this->_counter);
    _color.toHSV();
    this->setColor(_color);

    _expand += _up ? 0.015 * dt : -0.015 * dt;

    float trans    = _expand * 0.3f;

    // Translate subcurves
    //========================================================
    // north
    _local_curve[0]->translate({ _up ? trans : -trans, 0.0, 0.0 });
    // north-west
    _local_curve[1]->translate({ _up ? trans : -trans, 0.0, _up ? trans : -trans });
    // west
    _local_curve[2]->translate({0.0, 0.0, _up ? trans : -trans });
    // south-west
    _local_curve[3]->translate({ _up ? -trans : trans, 0.0, _up ? trans : -trans });
    // south
    _local_curve[4]->translate({ _up ? -trans : trans, 0.0, 0.0 });
    // south-east
    _local_curve[5]->translate({ _up ? -trans : trans, 0.0, _up ? -trans : trans });
    // east
    _local_curve[6]->translate({0.0, 0.0, _up ? -trans : trans });
    // north-east
    _local_curve[7]->translate({ _up ? trans : -trans, 0.0, _up ? -trans : trans });

}

#endif //GM_PARAMETRICS_CURVES_GERBSCURVE_H
}
