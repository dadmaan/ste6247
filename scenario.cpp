
#include <iostream>

#include "scenario.h"
// Curves
#include "code/butterfly.h"
#include "code/clover.h"
#include "code/reef.h"
#include "code/rose.h"
#include "code/bspline.h"
#include "code/blending.h"
#include "code/xfigure.h"
#include "code/asteroid.h"
#include "code/gerbscurve.h"
#include "code/gerbssurf.h"
#include "code/cylindericalsinewave.h"
#include "parametrics/curves/gmpbsplinecurve.h"
#include "parametrics/curves/gmpcircle.h"
#include "parametrics/curves/gmpbeziercurve.h"
// Surfaces
#include "parametrics/surfaces/gmpsphere.h"
#include "parametrics/surfaces/gmpcylinder.h"
#include "parametrics/surfaces/gmpplane.h"
#include "parametrics/surfaces/gmpdinisurface.h"
#include "testtorus.h"


// hidmanager
#include "hidmanager/defaulthidmanager.h"
// gmlib
#include <scene/light/gmpointlight.h>
#include <scene/sceneobjects/gmpathtrack.h>
#include <scene/sceneobjects/gmpathtrackarrows.h>

// qt
#include <QQuickItem>


template <typename T>
inline
std::ostream& operator<<(std::ostream& out, const std::vector<T>& v) {
    out << v.size() << std::endl;
    for(uint i=0; i<v.size(); i++) out << " " << v[i];
    out << std::endl;
    return out;
}




void Scenario::initializeScenario() {

    initializeCamera();

    /***************************************************************************
    *                                                                         *
    * Standar example, including path track and path track arrows             *
    *                                                                         *
    ***************************************************************************/

    GMlib::Material mm(GMlib::GMmaterial::polishedBronze());
    mm.set(45.0);

    // Scene 1
    simulation1();

    // Scene 2
//    simulation2();

    // Scene 3
//    simulation3();

    // Scene 4
//    simulation4();

    // Scene 5
//    simulation5();

}




void Scenario::cleanupScenario() {

}



void Scenario::simulation1()
{
    _bspline_one = prepareBspline(GMlib::GMcolor::yellow(),2);
    _bspline_two = prepareBspline(GMlib::GMcolor::purple(),5);
    _blend_curves = prepareBlending(_bspline_one,_bspline_two,0.4f);
}

void Scenario::simulation2()
{

    _circle = new GMlib::PCircle<float>(5);
    _circle->setColor(GMlib::GMcolor::yellow());
    _circle->rotateGlobal(M_PI / 2.0, { 1.0f, 0.0f, 0.0f });
    _circle->toggleDefaultVisualizer();
    _circle->sample(100, 0);
    this->scene()->insert(_circle);

    GMlib::DVector<GMlib::Vector<float, 3>> sample(100);
    float step = float(M_2PI / (sample.getDim() - 1));
    for (int i = 0; i < sample.getDim(); i++) {
      sample[i] = _circle->getPosition(i * step);
    }

    _sample_circle = new GMlib::BSpline<float>(sample, 10);
//    _sample_circle->rotate(GMlib::Angle(M_PI / 2.0), GMlib::Vector<float, 3>(1, 0, 0), false);
    _sample_circle->toggleDefaultVisualizer();
    _sample_circle->setColor(GMlib::GMcolor::darkMagenta());
    _sample_circle->sample(100, 0);
    this->scene()->insert(_sample_circle);

}

void Scenario::simulation3(){

    _asteroid = prepareAsteroid();
    _gerbs_curve.push_back(prepareGERBSCurve(_asteroid,4,GMlib::GMcolor::brown(),GMlib::SimulationMode::Asteroid));
    _gerbs_curve[0]->translateGlobal(GMlib::Vector<float,3>(0,0,0));
    _gerbs_curve[0]->rotateGlobal(GMlib::Angle(M_PI/4),GMlib::Vector<float,3>(1,0,0));

//    _cylinderical_sine_wave = prepareCylindericalSineWave();
//    _gerbs_curve.push_back(prepareGERBSCurve(_cylinderical_sine_wave,5,GMlib::GMcolor::black(),GMlib::SimulationMode::Sureal));
//    _gerbs_curve[0]->translateGlobal(GMlib::Vector<float,3>(0,0,0));

//    _xfigure = prepareXFigure();
//    _gerbs_curve.push_back(prepareGERBSCurve(_xfigure,8,GMlib::GMcolor::tan(),GMlib::SimulationMode::Expand));
//    _gerbs_curve[0]->translateGlobal(GMlib::Vector<float,3>(0,0,0));

//    _rose = prepareRose();
//    _gerbs_curve.push_back(prepareGERBSCurve(_rose,8,GMlib::GMcolor::yellow(),GMlib::SimulationMode::Rose));
//    _gerbs_curve[0]->translateGlobal(GMlib::Vector<float,3>(0,-10,10));

}

void Scenario::simulation4(){

    _dini = prepareDini();
    _gerbs_surface.push_back(prepareGERBSSurf(_dini,10,10,GMlib::GMmaterial::polishedRed(),GMlib::SurfaceShape::None));
    _gerbs_surface[0]->translateGlobal(GMlib::Vector<float,3>(0,10,-10));

}

void Scenario::simulation5(){

    _planet = preparePlanet();
    this->scene()->insert(_planet);
    _planet->translateGlobal(GMlib::Vector<float,3>(3,13,-3.5));

    _plane = preparePlane();
    _gerbs_surface.push_back(prepareGERBSSurf(_plane,10,10,GMlib::GMmaterial::polishedRed(),GMlib::SurfaceShape::Plane));
    _gerbs_surface[0]->translateGlobal(GMlib::Vector<float,3>(-5,10,0));
    _gerbs_surface[0]->scale(GMlib::Vector<float,3>(1.1f,1.1f,1.1f));

    _cylinder = prepareCylinder();
    _gerbs_surface.push_back(prepareGERBSSurf(_cylinder,10,10,GMlib::GMmaterial::polishedRed(),GMlib::SurfaceShape::Cylinder));
    _gerbs_surface[1]->translateGlobal(GMlib::Vector<float,3>(-3,10,-11));

    _torus = prepareTorus();
    _gerbs_surface.push_back(prepareGERBSSurf(_torus,10,10,GMlib::GMmaterial::polishedRed(),GMlib::SurfaceShape::Torus));
    _gerbs_surface[2]->rotateGlobal(M_PI/4,GMlib::Vector<float,3>(0,1,0));
    _gerbs_surface[2]->translateGlobal(GMlib::Vector<float,3>(-3,3,-14));

}

void Scenario::callDefferedGL() {

    GMlib::Array< const GMlib::SceneObject*> e_obj;
    this->scene()->getEditedObjects(e_obj);

    for(int i=0; i < e_obj.getSize(); i++)
        if(e_obj(i)->isVisible()) e_obj[i]->replot();
}
//********************************************
// Models Prepration
//********************************************

GMlib::PCurve<float,3>* Scenario::prepareBspline(const GMlib::Color& color,
                                                 int input,
                                                 int n){

    GMlib::DVector<GMlib::Vector<float,3>> points = generatePoints(input);
    GMlib::PCurve<float,3>* curve = new GMlib::BSpline<float>(points,n);
    curve->toggleDefaultVisualizer();
    curve->setColor(color);
    curve->rotateGlobal(-45,GMlib::Vector<float,3>(1,0,0));
    curve->translateGlobal(GMlib::Vector<float,3>(0,-7,-3));
    curve->setLineWidth(3);
    curve->sample(100,0);
    curve->showSelectors(0.5);
    this->scene()->insert(curve);

    return curve;
}

GMlib::PCurve<float,3>* Scenario::prepareCylindericalSineWave(){

    GMlib::PCurve<float,3>* curve = new GMlib::CylindericalSineWave<float>();
    curve->toggleDefaultVisualizer();
    curve->sample(300,0);
    return curve;
}

GMlib::PCurve<float,3>* Scenario::prepareBlending(GMlib::PCurve<float,3>* &curve1,
                                                  GMlib::PCurve<float,3>* &curve2,
                                                  float t_blend)
{
    GMlib::PCurve<float,3>* blend = new GMlib::PBlend<float>(curve1,curve2,t_blend);
    blend->toggleDefaultVisualizer();
    blend->sample(100,0);
    this->scene()->insert(blend);
    return blend;
}

GMlib::PCurve<float,3>* Scenario::prepareClover()
{
    GMlib::PCurve<float,3>* curve = new GMlib::PClover<float>;
    curve->toggleDefaultVisualizer();
    curve->sample(300,0);
    return curve;

}

GMlib::PCurve<float,3> *Scenario::prepareRose()
{
    GMlib::PCurve<float,3>* curve = new GMlib::PRose<float>;
    curve->toggleDefaultVisualizer();
    curve->sample(300,0);
    return curve;
}

GMlib::PCurve<float,3> *Scenario::prepareReef()
{
    GMlib::PCurve<float,3>* curve = new GMlib::PReef<float>;
    curve->toggleDefaultVisualizer();
    curve->sample(300,0);
    return curve;
}

GMlib::PCurve<float,3> *Scenario::prepareAsteroid()
{
    GMlib::PCurve<float,3>* curve = new GMlib::PAsteroid<float>;
    curve->toggleDefaultVisualizer();
    curve->sample(100,0);
    return curve;
}

GMlib::PCurve<float,3>* Scenario::prepareButterfly()
{
    GMlib::PCurve<float,3>* curve = new GMlib::PButterfly<float>();
    curve->toggleDefaultVisualizer();
    curve->sample(2000,1);
    this->scene()->insert(curve);
    return curve;
}

GMlib::PCurve<float,3>* Scenario::prepareXFigure(float radius)
{
    GMlib::PCurve<float,3>* curve = new GMlib::XFigure<float>(radius);
    curve->toggleDefaultVisualizer();
    curve->sample(100,0);
    return curve;
}

GMlib::GERBSCurve<float>* Scenario::prepareGERBSCurve(GMlib::PCurve<float,3>* &curve,
                                                      int n,
                                                      const GMlib::Color& color,
                                                      GMlib::SimulationMode sm)
{
    GMlib::GERBSCurve<float>* gerbs = new GMlib::GERBSCurve<float>(curve,n,sm);
    gerbs->toggleDefaultVisualizer();
    gerbs->setColor(color);
    gerbs->translate(GMlib::Vector<float,3>(0,0,0));
    gerbs->sample(300,0);
    this->scene()->insert(gerbs);
    return gerbs;
}

GMlib::PSurf<float,3>* Scenario::preparePlanet()
{
    GMlib::PSurf<float,3>* planet = new GMlib::PSphere<float>(1.0);
    planet->toggleDefaultVisualizer();
    planet->setMaterial(GMlib::GMmaterial::polishedGreen());
    planet->sample(25,25,3,3);

    GMlib::PCurve<float,3>* orbit = new GMlib::PCircle<float>(2.0);
    orbit->toggleDefaultVisualizer();
    orbit->sample(100,0);
    orbit->rotate(45,GMlib::Vector<float,3>(0,1,0));

    GMlib::PCurve<float,3>* orbit_1 = new GMlib::PCircle<float>(2.0);
    orbit_1->toggleDefaultVisualizer();
    orbit_1->sample(100,0);
    orbit_1->rotate(-65,GMlib::Vector<float,3>(0,1,0));

    auto gerbs = new GMlib::GERBSCurve<float>(orbit,2,GMlib::SimulationMode::Orbit);
    gerbs->toggleDefaultVisualizer();
    gerbs->setColor(GMlib::GMcolor::pink());
    gerbs->sample(200,0);
    planet->insert(gerbs);

    auto gerbs_1 = new GMlib::GERBSCurve<float>(orbit_1,2,GMlib::SimulationMode::Orbit);
    gerbs_1->toggleDefaultVisualizer();
    gerbs_1->rotateGlobal(GMlib::Angle(M_PI),GMlib::Vector<float,3>(0.0,0.5,1.0));
    gerbs_1->setColor(GMlib::GMcolor::pink());
    gerbs_1->sample(200,0);
    planet->insert(gerbs_1);

    return planet;
}

GMlib::PSurf<float,3>* Scenario::preparePlane()
{
    auto p = GMlib::Point<float,3>(0.0f, 0.0f, -5.0f);
    auto u = GMlib::Vector<float,3>(4,0,0);
    auto v = GMlib::Vector<float,3>(0,0,6);

    GMlib::PSurf<float,3>* surf = new GMlib::PPlane<float>(p,u,v);
    surf->toggleDefaultVisualizer();
    surf->rotate(M_PI/2,GMlib::Vector<float,3>(0,0,1));
    surf->sample(25,25,3,3);

    return surf;
}

GMlib::PSurf<float,3>* Scenario::prepareCylinder()
{
    GMlib::PSurf<float,3>* cylinder = new GMlib::PCylinder<float>(1.5f,1.5f,5.0f);
    cylinder->toggleDefaultVisualizer();
    cylinder->rotateGlobal(M_PI/2,GMlib::Vector<float,3>(0,1,0));
    cylinder->sample(25,25,3,3);

    return cylinder;
}

GMlib::PSurf<float,3>* Scenario::prepareDini()
{
    GMlib::PSurf<float,3>* dini = new GMlib::PDiniSurface<float>(1.5,.5);
    dini->toggleDefaultVisualizer();
    dini->rotateGlobal(M_PI/2,GMlib::Vector<float,3>(0,1,0));
    dini->sample(25,25,3,3);

    return dini;
}

GMlib::PSurf<float,3>* Scenario::prepareTorus()
{
    GMlib::PSurf<float,3>* torus = new GMlib::PTorus<float>(2.25f, 1.0f, 1.0f);
    torus->toggleDefaultVisualizer();
    torus->sample(25,25,3,3);

    return torus;
}

GMlib::GERBSSurf<float>* Scenario::prepareGERBSSurf(GMlib::PSurf<float, 3> *&model,
                                                    int n,
                                                    int m,
                                                    const GMlib::Material& material,
                                                    GMlib::SurfaceShape shape)
{
    GMlib::GERBSSurf<float>* gerbs = new GMlib::GERBSSurf<float>(model,n,m,shape);
    gerbs->toggleDefaultVisualizer();
    gerbs->setMaterial(material);
    gerbs->sample(25,25,3,3);
    this->scene()->insert(gerbs);

    return gerbs;
}

GMlib::DVector<GMlib::Vector<float,3> > Scenario::generatePoints(int input)
{
    GMlib::DVector< GMlib::Vector<float,3> > p(8);
    for(int i = 0; i < p.getDim(); i++)
        p[i]= GMlib::Vector<double,3>(cos(i*input),2*sin(3*i*input),i+input);
    return p;

}

void Scenario::initializeCamera()
{

    // Insert a light
    GMlib::Point<GLfloat,3> init_light_pos( 2.0, 4.0, 10 );
    GMlib::PointLight *light = new GMlib::PointLight(  GMlib::GMcolor::white(), GMlib::GMcolor::white(),
                                                     GMlib::GMcolor::white(), init_light_pos );
    light->setAttenuation(0.8f, 0.002f, 0.0008f);
    this->scene()->insertLight( light, false );

    // Insert Sun
    this->scene()->insertSun();

    // Default camera parameters
    int init_viewport_size = 600;
    GMlib::Point<float,3>  init_cam_pos( 0.0f, 0.0f, 0.0f );
    GMlib::Vector<float,3> init_cam_dir( 0.0f, 1.0f, 0.0f );
    GMlib::Vector<float,3> init_cam_up(  1.0f, 0.0f, 0.0f );

    // Projection cam
    auto proj_rcpair = createRCPair("Projection");
    proj_rcpair.camera->set(init_cam_pos,init_cam_dir,init_cam_up);
    proj_rcpair.camera->setCuttingPlanes( 1.0f, 8000.0f );
    proj_rcpair.camera->rotateGlobal( GMlib::Angle(-45), GMlib::Vector<float,3>( 1.0f, 0.0f, 0.0f ) );
    proj_rcpair.camera->translateGlobal( GMlib::Vector<float,3>( 0.0f, -20.0f, 20.0f ) );
    scene()->insertCamera( proj_rcpair.camera.get() );
    proj_rcpair.renderer->reshape( GMlib::Vector<int,2>(init_viewport_size, init_viewport_size) );

}

//**********************************
//To take a sample of the curve
//**********************************
//    GMlib::DVector<GMlib::Vector<float,3>> clover_sample(20);
//    for (int i=0; i<20; i++) {
//        clover_sample[i] = (*spline)(spline->getParStart()+(i*spline->getParDelta())/19);
//        std::cout<<clover_sample[i]<<std::endl;
//    }

